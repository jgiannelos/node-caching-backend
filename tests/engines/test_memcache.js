"use strict";
const { describe, beforeEach, it } = require("mocha");
const testHelpers = require("../helpers");
const MemcacheEngine = require("../../lib/engines/memcache");
const assert = require("assert");

describe("Memcache engine tests", function () {
  beforeEach(async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await new Promise((resolve, reject) => {
      engine.client.flush(function (err, val) {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
    await engine.close();
  });

  it("Should SET/GET key - key doesn't exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    const res = await engine.get("testkey");
    assert.ok(res.toString() === "testvalue");
    await engine.close();
  });

  it("Should SET/GET key - key doesn't exist - using TTL", async function () {
    this.timeout(10000);
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey", "testvalue", 5);
    const before = await engine.get("testkey");
    assert.ok(before.toString() === "testvalue");
    await new Promise((resolve) => setTimeout(resolve, 6000));
    const after = await engine.get("testkey");
    assert.ok(after === null);
    await engine.close();
  });

  it("Should SET/GET key - key exists", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey", "testvalue");
    const before = await engine.get("testkey");
    assert.ok(before.toString() === "testvalue");
    await engine.set("testkey", "testnewvalue");
    const after = await engine.get("testkey");
    assert.ok(after.toString() === "testnewvalue");
    await engine.close();
  });

  it("Should SET/GET key - multiple keys exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey1", "testvalue1");
    await engine.set("testkey2", "testvalue2");
    await engine.set("testkey3", "testvalue3");
    const before = await engine.get("testkey1");
    assert.ok(before.toString() === "testvalue1");
    await engine.set("testkey1", "testnewvalue");
    const after = await engine.get("testkey1");
    assert.ok(after.toString() === "testnewvalue");
    await engine.close();
  });

  it("Should GET key - key doesn't exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    const res = await engine.get("nonexistingkey");
    assert.ok(res === null);
    await engine.close();
  });

  it("Should DELETE key - key exists", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    await engine.delete("testkey");
    const res = await engine.get("testkey");
    assert.ok(res === null);
    await engine.close();
  });

  it("Should DELETE key - multiples keys exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    const before = await engine.get("testkey1");
    assert.ok(before.toString(), "testvalue1");
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    await engine.delete("testkey1");
    const after = await engine.get("testkey1");
    assert.ok(after === null);
    await engine.close();
  });

  it("Should DELETE key - key doesn't exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.delete("nonexistingkey");
    const after = await engine.get("nonexistingkey");
    assert.ok(after === null);
    await engine.close();
  });

  it("Should POP key - key exists", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey", "testvalue");
    const res = await engine.pop("testkey");
    assert.ok(res.toString() === "testvalue");
    const after = await engine.get("testkey");
    assert.ok(after === null);
    await engine.close();
  });

  it("Should POP key - multiple keys exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    await engine.set("testkey1", "testvalue1");
    await engine.set("testkey2", "testvalue2");
    await engine.set("testkey3", "testvalue3");
    const res = await engine.pop("testkey1");
    assert.ok(res.toString() === "testvalue1");
    const after = await engine.get("testkey1");
    assert.ok(after === null);
    await engine.close();
  });

  it("Should POP key - key doesn't exist", async function () {
    const config = testHelpers.getTestMemcacheConfig();
    const engine = new MemcacheEngine(config);
    const res = await engine.pop("nonexistingkey");
    assert.ok(res === null);
    await engine.close();
  });
});
