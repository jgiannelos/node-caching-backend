"use strict";
const MemoryEngine = require("../../lib/engines/memory");
const { describe, it } = require("mocha");
const assert = require("assert");

describe("In-memory engine tests", function () {
  it("Should SET/GET key - key doesn't exist", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey", "testvalue");
    assert.ok("testkey" in engine.cache.data);
  });

  it("Should SET/GET key - key doesn't exist - using TTL", async function () {
    this.timeout(10000);
    const engine = new MemoryEngine();
    engine.set("testkey", "testvalue", 5);
    assert.ok(engine.get("testkey") === "testvalue");
    await new Promise((resolve) => setTimeout(resolve, 6000));
    assert.ok(engine.get("testkey") === undefined);
  });

  it("Should SET/GET key - key exists", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey", "testvalue");
    assert.ok(engine.get("testkey") === "testvalue");
    engine.set("testkey", "othervalue");
    assert.ok(engine.get("testkey") === "othervalue");
  });

  it("Should SET/GET key - multiple keys exist", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey1", "testvalue1");
    engine.set("testkey2", "testvalue2");
    engine.set("testkey3", "testvalue3");
    assert.ok(engine.get("testkey1") === "testvalue1");
    engine.set("testkey1", "othervalue");
    assert.ok(engine.get("testkey1") === "othervalue");
  });

  it("Should DELETE key - key exists", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey", "testvalue");
    assert.ok(engine.get("testkey") === "testvalue");
    engine.delete("testkey");
    assert.ok(engine.get("testkey") === undefined);
  });

  it("Should DELETE key - multiples keys exist", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey1", "testvalue1");
    engine.set("testkey2", "testvalue2");
    engine.set("testkey3", "testvalue3");
    assert.ok(engine.get("testkey1") === "testvalue1");
    engine.delete("testkey1");
    assert.ok(engine.get("testkey1") === undefined);
    assert.ok(engine.cache.keys().length === 2);
  });

  it("Should DELETE key - key doesn't exist", async function () {
    const engine = new MemoryEngine();
    engine.delete("testkey1");
    assert.ok(engine.get("testkey1") === undefined);
  });

  it("Should POP key - key exists", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey", "testvalue");
    const value = engine.pop("testkey");
    assert.ok(engine.get("testkey") === undefined);
    assert.ok(value === "testvalue");
  });

  it("Should POP key - multiple keys exist", async function () {
    const engine = new MemoryEngine();
    engine.set("testkey1", "testvalue1");
    engine.set("testkey2", "testvalue2");
    engine.set("testkey3", "testvalue3");
    const value = engine.pop("testkey1");
    assert.ok(engine.get("testkey1") === undefined);
    assert.ok(value === "testvalue1");
    assert.ok(engine.cache.keys().length === 2);
  });

  it("Should POP key - key doesn't exist", async function () {
    const engine = new MemoryEngine();
    assert.ok(engine.cache.keys().length === 0);
    const value = engine.pop("testkey1");
    assert.ok(value === undefined);
  });
});
