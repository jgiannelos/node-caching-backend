"use strict";
const cassandra = require("cassandra-driver");

function getTestCassandraConfig() {
  const hosts = process.env.NODE_CACHING_BACKEND_CASS_HOST || '["127.0.0.1"]';
  return {
    hosts: JSON.parse(hosts),
    localDc: process.env.NODE_CACHING_BACKEND_CASS_DC || "datacenter1",
    keyspace: process.env.NODE_CACHING_BACKEND_CASS_KEYSPACE || "tests",
    cacheTable: process.env.NODE_CACHING_BACKEND_CASS_TABLE || "cache",
    username: process.env.NODE_CACHING_BACKEND_CASS_USER || "cassandra",
    password: process.env.NODE_CACHING_BACKEND_CASS_PASS || "cassandra",
  };
}

function getTestMemcacheConfig() {
  return {
    uri: process.env.NODE_CACHING_BACKEND_MEMC_URI || "127.0.0.1:11211",
  };
}

async function setupCassandraEnv() {
  const config = getTestCassandraConfig();
  const keyspace = config.keyspace;
  const table = config.cacheTable;
  const client = new cassandra.Client({
    contactPoints: config.hosts,
    localDataCenter: config.localDc,
  });

  const replication = "{'class': 'SimpleStrategy', 'replication_factor': 1}";

  await client.execute(
    `CREATE KEYSPACE IF NOT EXISTS ${keyspace} WITH REPLICATION = ${replication}`
  );
  await client.execute(
    `CREATE TABLE ${keyspace}.${table} (key text PRIMARY KEY, value blob)`
  );
  await client.shutdown();
}

async function tearDownCassandraEnv() {
  const config = getTestCassandraConfig();
  const client = new cassandra.Client({
    contactPoints: config.hosts,
    localDataCenter: config.localDc,
  });
  await client.execute(`DROP KEYSPACE IF EXISTS ${config.keyspace}`);
  await client.shutdown();
}

module.exports = {
  setupCassandraEnv,
  tearDownCassandraEnv,
  getTestCassandraConfig,
  getTestMemcacheConfig,
};
