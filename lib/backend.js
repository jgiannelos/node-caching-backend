"use strict";

const CassandraEngine = require("./engines/cassandra");
const MemoryEngine = require("./engines/memory");
const MemcacheEngine = require("./engines/memcache");

const ENGINES = {
  cassandra: CassandraEngine,
  dummy: MemoryEngine,
  memcached: MemcacheEngine,
};

/**
 * Class to create a caching backend
 */
class CacheBackend {
  /**
   * @constructor
   * @param {Object} config caching engine config
   */
  constructor(config) {
    this.config = config;
    this.engine = new ENGINES[config.engine](config);
  }

  /**
   * Set value for a given key
   * @param {String} key Key to be set
   * @param {Buffer} value Value to be set
   * @param {number} ttl The TTL of the object in seconds. Defaults to no expiration time.
   * @returns {Promise<void>}
   */
  set(key, value, ttl) {
    this.engine.set(key, value, ttl);
  }

  /**
   * Get the value for a given key
   * @param {String} key Key to get
   * @returns {Promise<Buffer|null>} cached value or null if not existing key
   */
  get(key) {
    const res = this.engine.get(key);
    return res || null;
  }

  /**
   * Delete the cache object for a given key
   * @param {String} key Key to delete
   * @returns {Promise<void>}
   */
  delete(key) {
    this.engine.delete(key);
  }

  /**
   * Get the value and delete the cache object for a given key
   * @property {Function} pop Get and delete the value for a key
   * @param {String} key Key to get
   * @returns {Promise<Buffer|null>} cached value or null if not existing key
   */
  pop(key) {
    const res = this.engine.pop(key);
    return res || null;
  }
}

module.exports = CacheBackend;
