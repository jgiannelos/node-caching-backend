"use strict";

/**
 * Response caching middleware factory
 *
 * @param {number} ttl
 * @param {function} keyFunc
 */
const cachedMiddlewareFactory = (
  ttl = 0,
  keyFunc = (req) => req.originalUrl
) => {
  return async function (req, res, next) {
    const backend = req.app.cache.backend;
    const key = keyFunc(req);
    const value = await backend.get(key);
    if (value) {
      res.send(value.toString());
      return;
    }
    res.sendRes = res.send;
    res.send = async (body) => {
      await backend.set(key, body, ttl);
      res.sendRes(body);
    };
    next();
  };
};

module.exports = cachedMiddlewareFactory;
