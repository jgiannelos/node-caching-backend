"use strict";
/**
 * Require a module or throw custom error
 * @param {String} id The module to import
 * @returns {void}
 */
function requireEngineDependency(id) {
  try {
    return require(id);
  } catch (e) {
    const msg = `To enable support for this engine you need to install: ${id}`;
    console.log(msg);
    throw e;
  }
}

module.exports = {
  requireEngineDependency,
};
