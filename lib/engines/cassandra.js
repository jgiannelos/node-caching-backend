"use strict";
const fs = require("fs");
const requireEngineDependency = require("../helpers").requireEngineDependency;
const cassandra = requireEngineDependency("cassandra-driver");

const loadBalancing = cassandra.policies.loadBalancing;
const reconnection = cassandra.policies.reconnection;
const distance = cassandra.types.distance;
const AuthProvider = cassandra.auth.PlainTextAuthProvider;

class CassandraEngine {
  sslOptions(sslConf) {
    const sslOpts = {};

    if (sslConf.cert) {
      sslOpts.cert = fs.readFileSync(sslConf.cert);
    }
    if (sslConf.key) {
      sslOpts.key = fs.readFileSync(sslConf.key);
    }

    if (sslConf.ca) {
      sslOpts.ca = [];
      if (sslConf.ca instanceof Array) {
        sslConf.ca.forEach((ca) => {
          sslOpts.ca.push(fs.readFileSync(ca));
        });
      } else {
        sslOpts.ca.push(fs.readFileSync(sslConf.ca));
      }
    }

    return sslOpts;
  }

  constructor(config) {
    this.options = {
      keyspace: config.keyspace,
      contactPoints: config.hosts,
      policies: {
        loadBalancing: new loadBalancing.TokenAwarePolicy(
          new loadBalancing.DCAwareRoundRobinPolicy(config.localDc)
        ),
        reconnection: new reconnection.ExponentialReconnectionPolicy(
          100,
          120000,
          true
        ),
      },
      protocolOptions: {
        maxSchemaAgreementWaitSeconds: 30,
        maxVersion: 3,
      },
      authProvider: new AuthProvider(config.username, config.password),
      maxPrepared: config.maxPrepared || 50000,
    };

    if (config.tls) {
      config.sslOptions = this.sslConf(config.tls);
    }

    if (config.pooling) {
      this.options.pooling = {
        coreConnectionsPerHost: {
          [distance.local]: config.coreConnectionsLocalDC,
          [distance.remote]: config.coreConnectionsRemoteDC,
        },
      };
    }

    this.client = new cassandra.Client(this.options);
    this.table = config.cacheTable;
  }

  async set(key, value, ttl = 0) {
    const query = `UPDATE ${this.table} USING TTL ? SET value = ? WHERE key = ?`;
    const params = [ttl, value, key];
    await this.client.execute(query, params, { prepare: true });
  }

  async get(key) {
    const query = `SELECT value FROM ${this.table} WHERE key = ?`;
    const params = [key];
    const res = await this.client.execute(query, params, { prepare: true });
    const firstRes = res.first();

    if (firstRes) {
      return firstRes.value;
    }
    return null;
  }

  async delete(key) {
    const query = `DELETE FROM ${this.table} WHERE key = ?`;
    const params = [key];
    await this.client.execute(query, params, { prepare: true });
  }

  async pop(key) {
    const value = await this.get(key);
    if (value) {
      await this.delete(key);
    }
    return value;
  }

  async close() {
    await this.client.shutdown();
  }
}

module.exports = CassandraEngine;
