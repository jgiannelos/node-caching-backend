"use strict";
const requireEngineDependency = require("../helpers").requireEngineDependency;
const memjs = requireEngineDependency("memjs");

class MemcacheEngine {
  constructor(config) {
    const servers = config.uri;
    const options = {
      retries: config.retries || 3,
      failover: config.failover || false,
      failoverTime: config.failoverTime || 60,
    };
    this.client = memjs.Client.create(servers, options);
  }

  async set(key, value, ttl = 0) {
    await new Promise((resolve, reject) => {
      this.client.set(key, value, { expires: ttl }, function (err, val) {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
  }

  async get(key) {
    return await new Promise((resolve, reject) => {
      this.client.get(key, function (err, val) {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
  }

  async delete(key) {
    await new Promise((resolve, reject) => {
      this.client.delete(key, function (err, val) {
        if (err) {
          reject(err);
        } else {
          resolve(val);
        }
      });
    });
  }

  async pop(key) {
    const value = await this.get(key);
    if (value) {
      await this.delete(key);
    }
    return value;
  }

  async close() {
    this.client.quit();
  }
}

module.exports = MemcacheEngine;
