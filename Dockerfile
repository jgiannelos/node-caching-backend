FROM docker-registry.wikimedia.org/nodejs14-slim:latest
COPY . /app
WORKDIR /app
RUN npm install